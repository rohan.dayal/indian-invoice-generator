# Indian Invoice Generator

Project built with NextJS, MongoDB, Mongoose

**Indian Invoice Generator** is an application that allows users to create invoices for their business. It is still a WIP and was built as a learner project to understand the working of NextJS.

**Features**

* Create your invoice at `<server>`/new
* All saved invoices are seen at `<server>`/
* View invoice at `<server>`/`<id>`
* View invoice JSON at `<server>`/`<id>`?format=json
* View invoice PDF at `<server>`/`<id>`?format=PDF

## How to use

Clone this repository and test/deploy on your own server.
## Install and run:

npm install
npm run dev


## Configuration
### Step 1. Get the connection string of your MongoDB server

In the case of MongoDB Atlas, it should be a string like this:

```
mongodb+srv://<username>:<password>@my-project-abc123.mongodb.net/test?retryWrites=true&w=majority
```

**OR**

```
mongodb://localhost:27017?ssl=false
```

### Step 2. Set up environment variables

Copy the `.env.local.example` file in this directory to `.env.local` (which will be ignored by Git):

```bash
cp .env.local.example .env.local
```

Then set each variable on `.env.local`:

- `MONGODB_URI` should be the MongoDB connection string you got from step 1.

### Step 3. Run Next.js in development mode

```bash
npm install
npm run dev

# or

yarn install
yarn dev
```