import mongoose from 'mongoose'
import {CompanySchema} from './company';

export const InvoiceSchema = new mongoose.Schema({
  invoiceNumber: {
    type: String,
    required: [true, 'Invoice number is required'],
    maxlength: [20, 'Name cannot be more than 20 characters'],
    inputType: 'string'
  },
  invoiceDate: {
    type: Date,
    required: [true, "Invoice date is required"],
    default: Date.now,
    inputType: 'date'
  },
  dueDate: {
    type: Date,
    required: [true, 'Invoice due date is required'],
    inputType: 'date'
  },
  biller: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Company',
  },
  buyer: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Company',
  },
  currencyCode: {
    type: String,
    inputType: 'string',
    maxLength: [3, "Currency code can be 3 characters at most"]
  },
  items: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'List'
  }],
  tnc: {
    type: [String],
    default: []
  },
  signature: {
    /* URL of the signature as uploaded */
    type: String,
  },
  charges: {
    type: Array
  },
  discounts: {
    type: Array
  }
})

export default mongoose.models.Invoice || mongoose.model('Invoice', InvoiceSchema);