import mongoose from 'mongoose';

const validateEmail = function(email) {
    var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return re.test(email)
};

export const CompanySchema = new mongoose.Schema({
    name: {
        type: String,
        trim: true,
        required: [true, 'Company name is required'],
        maxLength: [60, 'Company name cannot be more than 60 characters'],
        inputType: 'string'
    },
    email: {
        type: String,
        trim: true,
        lowercase: true,
        validate: [validateEmail, 'Please provide a valid email address'],
        match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/, 'Please fill a valid email address'],
        inputType: 'email'
    },
    phone: {
        type: Number,
        trim: true,
        required: [true, 'Company phone is required'],
        inputType: 'number'
    },
    gst: {
        type: String,
        trim: true,
        inputType: 'string'
    },
    pan: {
        type: String,
        trim: true,
        maxLength: [10, "PAN cannot be longer than 10 characters"],
        inputType: 'string'
    },
    address: {
        type: String,
        trim: true,
        maxLength: [100, 'Address cannot be more than 100 characters'],
        inputType: 'string'
    },
    city: {
        type: String,
        trim: true,
        maxLength: [30, 'City cannot be more than 30 characters'],
        inputType: 'string'
    },
    state: {
        type: String,
        trim: true,
        maxLength: [30, 'State cannot be more than 30 characters'],
        inputType: 'string'
    },
    areaCode: {
        type: Number,
        trim: true,
        maxLength: [10, 'Area code cannot be more than 30 characters'],
        inputType: 'number'
    },
});

export default mongoose.models.Company || mongoose.model('Company', CompanySchema);