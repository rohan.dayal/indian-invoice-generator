import mongoose from 'mongoose';

export const DiscountSchema = new mongoose.Schema({
    discountType: {
        type: String,
        inputType: 'option',
        default: 'percentage'
    },
    amount: {
        type: Number,
        inputType: 'number',
    },
});

export default mongoose.models.Discount || mongoose.model('Discount', DiscountSchema);