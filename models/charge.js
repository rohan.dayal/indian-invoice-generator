import mongoose from 'mongoose';

export const ChargeSchema = new mongoose.Schema({
    name: {
        type: String,
        inputType: 'string'
    },
    amount: {
        type: Number,
        inputType: 'number'
    }
})

export default mongoose.models.Charge || mongoose.model('Charge', ChargeSchema);