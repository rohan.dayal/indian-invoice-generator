import mongoose from 'mongoose'

export const ItemSchema = new mongoose.Schema({
    name: {
        type: String,
        trim: true,
        required: [true, 'Item name is required'],
        maxLength: [60, 'Item name cannot be more than 60 characters'],
        inputType: 'string'
    },
    hsn: {
        type: String,
        trim: true,
        maxLength: [100, 'HSN cannot be more than 100 characters'],
        inputType: 'string'
    },
    gst: {
        type: Number,
        trim: true,
        maxLength: [100, 'HSN cannot be more than 100 characters'],
        inputType: 'number'
    },
    quantity: {
        type: Number,
        trim: true,
        default: 0,
        inputType: 'number'
    },
    rate: {
        type: Number,
        trim: true,
        default: 0,
        inputType: 'number'
    },
    description: {
        type: String,
        trim: true,
        maxLength: [60, 'Item name cannot be more than 60 characters'],
        inputType: 'string'
    },
    discounts: {
        type: Array
    }
});

export default mongoose.models.Item || mongoose.model('Item', ItemSchema);