import { useState } from 'react';
import { useRouter } from 'next/router';
import { mutate } from 'swr';
import {Row, Col, Form, Input, InputNumber, Button} from 'antd';
import {FileImageOutlined, CloseCircleOutlined} from '@ant-design/icons';

import DatePicker from '../components/DatePicker';
import CompanyForm from './CompanyForm';

const InvoiceForm = ({ formId, invoiceForm, forNewInvoice = true }) => {
  const router = useRouter()
  const contentType = 'application/json'
  const [errors, setErrors] = useState({})
  const [message, setMessage] = useState('')

  const [form, setForm] = useState({
    invoice: {...invoiceForm.invoice},
    biller: {...invoiceForm.biller},
    buyer: {...invoiceForm.buyer},
    items: [...invoiceForm.items],
  })

  /* The PUT method edits an existing entry in the mongodb database. */
  const putData = async (form) => {
    const { id } = router.query

    try {
      const res = await fetch(`/api/invoices/${id}`, {
        method: 'PUT',
        headers: {
          Accept: contentType,
          'Content-Type': contentType,
        },
        body: JSON.stringify(form),
      })

      // Throw error with status code in case Fetch API req failed
      if (!res.ok) {
        throw new Error(res.status)
      }

      const { data } = await res.json()

      mutate(`/api/invoices/${id}`, data, false) // Update the local data without a revalidation
      router.push('/')
    } catch (error) {
      setMessage('Failed to update invoice')
    }
  }

  /* The POST method adds a new entry in the mongodb database. */
  const postData = async (form) => {
    try {
      const res = await fetch('/api/invoices', {
        method: 'POST',
        headers: {
          Accept: contentType,
          'Content-Type': contentType,
        },
        body: JSON.stringify(form),
      })

      // Throw error with status code in case Fetch API req failed
      if (!res.ok) {
        throw new Error(res.status)
      }

      router.push('/')
    } catch (error) {
      setMessage('Failed to add invoice')
    }
  }

  const addRow = () => {
    let items = [...form.items];
    items.push({
      gst: 18,
      quantity: 1,
      rate: 1,
    });
    setForm({...form, items: items});
  }

  const removeRow = (index) => {
    let items = [...form.items];
    items.splice(index, 1);
    setForm({...form, items: items});
  }

  const handleChange = (name, value, type=null, index=null) => {
    if(!type) {
      let {invoice} = form;
      invoice[name] = value;
      setForm({...form, invoice: invoice})
    } else if(type === 'item') {
      let items = [...form.items];
      items[index][name] = value;
      setForm({...form, items: items})
    } else {
      let subData = {...form[type]};
      subData[name] = value;
      setForm({...form, [type]: subData})
    }
  }

  const handleSubmit = (e) => {
    // e.preventDefault()
    const errs = formValidate()
    if (Object.keys(errs).length === 0) {
      forNewInvoice ? postData(form) : putData(form)
    } else {
      setErrors({ errs })
    }
  }

  /* Makes sure pet info is filled for pet name, owner name, species, and image url*/
  const formValidate = () => {
    let err = {}
    // TODO: need to add validation
    return err
  }

  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 8 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 16 },
    },
  };

  return (
    <>
      <Form
        // layout="vertical"
        id={formId}
        onFinish={handleSubmit}
      >
        <Row>
          <Col xs={24} md={12} style={{padding: '0px 10px'}}>
            <Form.Item {...formItemLayout} label="Invoice Number">
                <Input placeholder="Unique invoice number" value={form.invoice.invoiceNumber} onChange={(e) => handleChange(e.target.name, e.target.value)} name="invoiceNumber" />
            </Form.Item>
            <Form.Item {...formItemLayout} label="Invoice Date">
              <DatePicker value={form.invoice.invoiceDate} name="invoiceDate" style={{width: '100%'}} onChange={(dateVal) => handleChange('invoiceDate', dateVal)} />
            </Form.Item>
            <Form.Item {...formItemLayout} label="Due Date">
              <DatePicker value={form.invoice.dueDate} name="dueDate" style={{width: '100%'}} onChange={(dateVal) => handleChange('dueDate', dateVal)} />
            </Form.Item>
          </Col>
          <Col xs={24} md={12} style={{textAlign: 'center', padding: '0px 40px'}}>
            <div style={{background: 'lightgrey', border: '1px solid grey', display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center', padding: '33px 40px'}}>
              <div>
                <FileImageOutlined style={{ fontSize: '30px', color: 'grey' }} /><br />
                Upload Company Logo: Coming Soon
              </div>
            </div>
          </Col>
          <CompanyForm company={form.biller} handleChange={handleChange} type='biller' formItemLayout={formItemLayout} />
          <CompanyForm company={form.buyer} handleChange={handleChange} type='buyer' formItemLayout={formItemLayout} />
          <Col xs={24} md={12} style={{padding: '10px 10px 0px'}}>
            <Form.Item {...formItemLayout} label="Currency Code">
                <Input placeholder="INR" value={form.invoice.currencyCode} onChange={(e) => handleChange(e.target.name, e.target.value)} name="currencyCode" />
            </Form.Item>
          </Col>
          <Col xs={24} md={12} style={{padding: '10px 10px 0px'}}></Col>
          <Col xs={24} md={24} style={{padding: '10px 10px 0px'}}>
            <Row className="item-table-header-row">
              <Col xs={6} className="item-table-header-cell">Item</Col>
              <Col xs={3} className="item-table-header-cell">HSN/SAC</Col>
              <Col xs={2} className="item-table-header-cell">GST</Col>
              <Col xs={2} className="item-table-header-cell">Quantity</Col>
              <Col xs={2} className="item-table-header-cell">Rate</Col>
              <Col xs={2} className="item-table-header-cell">Amount</Col>
              <Col xs={2} className="item-table-header-cell">CGST</Col>
              <Col xs={2} className="item-table-header-cell">SGST</Col>
              <Col xs={2} className="item-table-header-cell">Total</Col>
              <Col xs={1} className="item-table-header-cell"></Col>
            </Row>
            { form.items.map((item, index) => (
              <Row className="item-table-row" key={index.toString()}>
                <Col xs={6} className="item-table-cell">
                  <Input value={item.name} onChange={(e) => handleChange(e.target.name, e.target.value, 'item', index)} name="name" style={{width: '100%'}} />
                </Col>
                <Col xs={3} className="item-table-cell">
                  <Input value={item.hsn} onChange={(e) => handleChange(e.target.name, e.target.value, 'item', index)} name="hsn" style={{width: '100%'}} />
                </Col>
                <Col xs={2} className="item-table-cell">
                  <InputNumber value={item.gst} onChange={(e) => handleChange("gst", e, 'item', index)} name="gst" style={{width: '100%'}} />
                </Col>
                <Col xs={2}>
                  <InputNumber value={item.quantity} onChange={(e) => handleChange('quantity', e, 'item', index)} name="quantity" style={{width: '100%'}} />
                </Col>
                <Col xs={2} className="item-table-cell">
                  <InputNumber value={item.rate} onChange={(e) => handleChange('rate', e, 'item', index)} name="rate" style={{width: '100%'}} />
                </Col>
                <Col xs={2} className="item-table-cell item-table-cell-static">
                  {form.currencyCode} {isNaN(item.quantity*item.rate) ? '-' : item.quantity*item.rate}
                </Col>
                <Col xs={2} className="item-table-cell item-table-cell-static">
                  {form.currencyCode} {isNaN(item.quantity*item.rate*item.gst/200) ? '-' : (item.quantity*item.rate*item.gst/200)}
                </Col>
                <Col xs={2} className="item-table-cell item-table-cell-static">
                  {form.currencyCode} {isNaN(item.quantity*item.rate*item.gst/200) ? '-' : (item.quantity*item.rate*item.gst/200)}
                </Col>
                <Col xs={2} className="item-table-cell item-table-cell-static">
                  {form.currencyCode} {isNaN(item.quantity*item.rate*(1+(item.gst/100))) ? '-' : Math.round((item.quantity*item.rate*(1+(item.gst/100)))*100)/100}
                </Col>
                <Col xs={1} className="item-table-cell item-table-cell-static" style={{cursor: 'pointer'}}><CloseCircleOutlined onClick={() => removeRow(index)} /></Col>
              </Row>
            ))}
          </Col>
          <Col xs={24} style={{padding: '20px 10px 0px'}}>
            <Button type="default" onClick={addRow}>+ Add Item</Button>
          </Col>
          <Col xs={24} style={{padding: '20px 10px 0px', textAlign: 'center'}}>
            <Form.Item>
                <Button type="primary" htmlType="submit">Submit</Button>
            </Form.Item>
          </Col>
        </Row>
      </Form>
      <p>{message}</p>
      <div>
        {Object.keys(errors).map((err, index) => (
          <li key={index}>{err}</li>
        ))}
      </div>
    </>
  )
}

export default InvoiceForm
