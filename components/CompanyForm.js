import {Row, Col, Form, Input} from 'antd';

const CompanyForm = ({ company, handleChange, type='biller', formItemLayout }) => {
    return (
        <>
            <Col xs={24} md={12} style={{padding: '0px 10px', background: '#dcdcef'}}>
                <div style={{fontSize: '20px', textAlign: 'center', padding: '10px 0px'}}>{type === 'biller' ? 'Billed By' : 'Billed To'}</div>
                <Form.Item {...formItemLayout} label="Name">
                    <Input placeholder="Name" value={company.name} onChange={(e) => handleChange(e.target.name, e.target.value, type)} name="name" />
                </Form.Item>
                <Form.Item {...formItemLayout} label="Email">
                    <Input placeholder="abc@xyz.com" value={company.email} onChange={(e) => handleChange(e.target.name, e.target.value, type)} name="email" />
                </Form.Item>
                <Form.Item {...formItemLayout} label="Phone">
                    <Input placeholder="9999999999" value={company.phone} onChange={(e) => handleChange(e.target.name, e.target.value, type)} name="phone" />
                </Form.Item>
                <Form.Item {...formItemLayout} label="GST">
                    <Input placeholder="AAAAA1111A" value={company.gst} onChange={(e) => handleChange(e.target.name, e.target.value, type)} name="gst" />
                </Form.Item>
                <Form.Item {...formItemLayout} label="Address">
                    <Input placeholder="Address" value={company.address} onChange={(e) => handleChange(e.target.name, e.target.value, type)} name="address" />
                </Form.Item>
                <Form.Item {...formItemLayout} label="City">
                    <Input placeholder="City" value={company.city} onChange={(e) => handleChange(e.target.name, e.target.value, type)} name="city" />
                </Form.Item>
                <Form.Item {...formItemLayout} label="State">
                    <Input placeholder="State" value={company.state} onChange={(e) => handleChange(e.target.name, e.target.value, type)} name="state" />
                </Form.Item>
                <Form.Item {...formItemLayout} label="Area Code">
                    <Input placeholder="111111" value={company.areaCode} onChange={(e) => handleChange(e.target.name, e.target.value, type)} name="areaCode" />
                </Form.Item>
            </Col>
        </>
    )
}

export default CompanyForm;