import React from 'react';

const InvoiceView = ({invoice}) => {
    console.log(invoice);
    return(
        <>
            <div style={{textAlign: 'center', fontWeight: 'bold', fontSize: '22px'}}>INVOICE</div>
            <table>
                <tbody>
                    <tr>
                        <td style={{width: '50%'}}>
                            <div>{invoice.biller.name}</div>
                            <div>{invoice.biller.address}</div>
                            <div>{invoice.biller.city} - {invoice.biller.areaCode}</div>
                            <div>{invoice.biller.state}</div>
                            <div>Email: {invoice.biller.email}</div>
                            <div>Phone: {invoice.biller.phone}</div>
                            <div>GST: {invoice.biller.phone}</div>
                        </td>
                        <td style={{width: '50%'}}>
                            <div style={{textAlign: 'right'}}>Invoice Number: {invoice.invoiceNumber}</div>
                            <div style={{textAlign: 'right'}}>Date: {invoice.invoiceDate}</div>
                            <div style={{textAlign: 'right'}}>Due Date: {invoice.dueDate}</div>
                        </td>
                    </tr>
                </tbody>
            </table>
            <div>Bill To:</div>
            <div>{invoice.buyer.name}</div>
            <div>{invoice.buyer.address}</div>
            <div>{invoice.buyer.city} - {invoice.buyer.areaCode}</div>
            <div>{invoice.buyer.state}</div>
            <div>Email: {invoice.buyer.email}</div>
            <div>Phone: {invoice.buyer.phone}</div>
            <div>GST: {invoice.buyer.phone}</div>
            <table style={{width: '100%'}}>
                <thead>
                    <tr>
                        <th>Item</th>
                        <th>HSN</th>
                        <th>GST</th>
                        <th>Quantity</th>
                        <th>Rate</th>
                        <th>Amount</th>
                        <th>CGST</th>
                        <th>SGST</th>
                        <th>Total</th>
                    </tr>
                </thead>
                <tbody>
                    { invoice.items.map((item, index) => (
                        <tr>
                            <td>{item.name}</td>
                            <td>{item.hsn}</td>
                            <td>{item.gst}%</td>
                            <td>{item.quantity}</td>
                            <td>{invoice.currencyCode} {item.rate}</td>
                            <td>{invoice.currencyCode} {item.quantity*item.rate}</td>
                            <td>{invoice.currencyCode} {item.quantity*item.rate*item.gst/200}</td>
                            <td>{invoice.currencyCode} {item.quantity*item.rate*item.gst/200}</td>
                            <td>{invoice.currencyCode} {Math.round((item.quantity*item.rate*(1+(item.gst/100)))*100)/100}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </>
    )
}

export default InvoiceView;