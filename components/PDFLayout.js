import React from 'react';
import '../css/pdf.css';

const PDFLayout = ({children}) => (
    <html>
        <head>
            <meta charSet="utf8" />
        </head>
        <body>
            {children}
        </body>
    </html>
)

export default PDFLayout;