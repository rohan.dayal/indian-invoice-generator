import '../css/style.css'
import 'antd/dist/antd.css'
import Head from 'next/head'
import Link from 'next/link'
import {Layout} from 'antd';

const {Header, Content} = Layout;

function MyApp({ Component, pageProps }) {
  return (
    <>
      <Head>
        <title>Create free invoices</title>
      </Head>

      <Layout className="layout">
        <Header>
          <div className="site-header">.: Invoice Creator :.</div>
        </Header>
        <Content style={{ padding: '20px 10% 0px' }}>
          <Component {...pageProps} />
        </Content>
      </Layout>
    </>
  )
}

export default MyApp
