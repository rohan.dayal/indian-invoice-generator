import dbConnect from '../../../utils/dbConnect'
import Invoice from '../../../models/invoice';
import Company from '../../../models/company';
import Item from '../../../models/item';

export default async function handler(req, res) {
  const { method } = req

  await dbConnect()

  switch (method) {
    case 'GET':
      try {
        const invoices = await Invoice.find({}).populate('biller').populate('buyer').populate('items') /* find all the data in our database */
        res.status(200).json({ success: true, data: invoices })
      } catch (error) {
        res.status(400).json({ success: false })
      }
      break
    case 'POST':
      try {
        const biller = await Company.create(req.body.biller);
        const buyer = await Company.create(req.body.buyer)
        const items = await Item.insertMany(req.body.items);
        const invoice = {...req.body.invoice};
        invoice.biller = biller._id;
        invoice.buyer = buyer._id;
        invoice.items = [];
        for(var i=0; i<items.length; i++) {
          invoice.items.push(items[i]._id);
        }
        const newInvoice = await Invoice.create(
          invoice
        ) /* create a new model in the database */
        res.status(201).json({ success: true, data: newInvoice })
      } catch (error) {
        console.log(error);
        res.status(400).json({ success: false })
      }
      break
    default:
      res.status(400).json({ success: false })
      break
  }
}
