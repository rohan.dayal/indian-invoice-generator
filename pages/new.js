import InvoiceForm from '../components/Form'

const NewInvoice = () => {
  const invoiceForm = {
    invoice: {
      invoiceNumber: '',
      invoiceDate: null,
      dueDate: null,
      currencyCode: 'INR',
      discount: {},
      charges: [],
      tnc: [
        'Please pay within 15 days from the date of invoice, overdue interest @ 14% will be charged on delayed payments.', 
        'Please quote invoice number when remitting funds.'
      ],
    },
    biller: {},
    buyer: {},
    items: [{
      gst: 18,
      quantity: 1,
      rate: 1,
    }],
  }

  return <InvoiceForm formId="add-invoice-form" invoiceForm={invoiceForm} />
}

export default NewInvoice;