import Link from 'next/link'
import dbConnect from '../utils/dbConnect'
import Invoice from '../models/invoice';

const Index = ({ invoices }) => (
  <>
    {/* Create a card for each pet */}
    {invoices.map((invoice) => (
      <div key={invoice._id}>
        <div className="card">
          <h5 className="invoice-name">{invoice.invoiceNumber}</h5>
        </div>
      </div>
    ))}
  </>
)

/* Retrieves pet(s) data from mongodb database */
export async function getServerSideProps() {
  await dbConnect()

  /* find all the data in our database */
  const result = await Invoice.find({}).populate('biller').populate('buyer').populate({path: 'items', model: 'Item'}).exec();
  const invoices = result.map((doc) => {
    const invoice = doc.toObject({versionKey: false})
    invoice._id = invoice._id.toString();
    invoice.dueDate = invoice.dueDate.toString();
    invoice.invoiceDate = invoice.invoiceDate.toString();
    invoice.biller._id = invoice.biller._id.toString();
    invoice.buyer._id = invoice.buyer._id.toString();
    for(var i=0; i<invoice.items.length; i++) {
      invoice.items[i]._id = invoice.items[i]._id.toString();
    }
    return invoice
  })

  return { props: { invoices: invoices } }
}

export default Index