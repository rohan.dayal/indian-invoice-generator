import { useRouter } from 'next/router'
import useSWR from 'swr'
import Form from '../../components/Form'
import dayjs from 'dayjs';

const fetcher = (url) =>
  fetch(url)
    .then((res) => res.json())
    .then((json) => json.data)

const EditInvoice = () => {
  const router = useRouter()
  const { id } = router.query
  const { data: invoice, error } = useSWR(id ? `/api/invoices/${id}` : null, fetcher)

  if (error) return <p>Failed to load</p>
  if (!invoice) return <p>Loading...</p>

  const invoiceForm = {
    invoice: {
      invoiceNumber: invoice.invoiceNumber,
      invoiceDate: dayjs(invoice.invoiceDate),
      dueDate: dayjs(invoice.dueDate),
      currencyCode: invoice.currencyCode,
      discount: invoice.discount,
      charges: invoice.charges,
      tnc: invoice.tnc,
      signature: invoice.signature,
    },
    biller: invoice.biller,
    buyer: invoice.buyer,
    items: invoice.items,
  }

  return <Form formId="edit-pet-form" invoiceForm={invoiceForm} forNewPet={false} />
}

export default EditInvoice
